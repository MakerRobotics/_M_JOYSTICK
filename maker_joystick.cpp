  /*
 * Library: maker_joystick.h
 *
 * Organization: MakerRobotics
 * Autors: Maksim Jovovic
 *
 * Date: 08.12.2017.
 * Test: Arduino UNO
 *
 * Note:
 *
 */

#include "Arduino.h"
#include "maker_joystick.h"
#include "stdint.h"

uint8_t x_AnalogPin;
uint8_t y_AnalogPin;
uint8_t switch_DigitalPin;

/* Sets up the pins used for input from the joystick */
void maker_initializeJoystick(uint8_t new_x_AnalogPin, uint8_t new_y_AnalogPin, uint8_t new_switch_DigitalPin)
{
    x_AnalogPin = new_x_AnalogPin;
    y_AnalogPin = new_y_AnalogPin;
    switch_DigitalPin = new_switch_DigitalPin;

    pinMode(switch_DigitalPin, 0);
    digitalWrite(switch_DigitalPin, 1);
}

/* Returns the x input from the joystick */
uint16_t maker_getJoystickInputX()
{
    return analogRead(x_AnalogPin);
}

/* Returns the y input from the joystick */
uint16_t maker_getJoystickInputY()
{
    return analogRead(y_AnalogPin);
}

/* Returns the switch input from the joystick */
uint8_t maker_getJoystickInputSwitch()
{
    return digitalRead(switch_DigitalPin);
}
