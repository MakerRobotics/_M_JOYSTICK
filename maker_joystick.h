#ifndef _MAKER_JOYSTICK_
#define _MAKER_JOYSTICK_

extern uint8_t x_AnalogPin;
extern uint8_t y_AnalogPin;
extern uint8_t switch_DigitalPin;

/**
    Note: Sets up the pins used for input from the joystick

    @param
        new_x_AnalogPin is the analog pin from which to read the x value.
        new_y_AnalogPin is the analog pin from which to read the y value.
        new_switch_DigitalPin is the digital pin from which to read the switch value.

    @return
      none
*/
void maker_initializeJoystick(uint8_t new_x_AnalogPin, uint8_t new_y_AnalogPin, uint8_t new_switch_DigitalPin);

/**
    Note: Returns the x input from the joystick

    @param
      none

    @return
      the x input value between 0 & 1024
*/
uint16_t maker_getJoystickInputX();

/**
    Note: Returns the y input from the joystick

    @param
      none

    @return
      the y input value between 0 & 1024
*/
uint16_t maker_getJoystickInputY();

/**
    Note: Returns the switch input from the joystick

    @param
        none

    @return
      the switch input value (0 - pressed, 1 - released).
*/
uint8_t maker_getJoystickInputSwitch();

#endif
